# Missioni Scratch

## Onboarding

Queste missioni non fanno riferimento alle giornate.
Se un partecipante non ha mai utilizzato Scratch segue questa lista.
Una volta completata la lista puo passare alle missioni giornaliere.

1. Completa il tutorial Scratch.
2. Impara il funzionamento dei blocchi.
3. Progetto finale.

## Missioni Giornaliere

### 2021-09-06
#### Difficile
#### Normale

### 2021-09-07
#### Difficile
#### Normale

### 2021-09-08
#### Difficile
#### Normale

### 2021-09-09
#### Difficile
#### Normale

### 2021-09-10
#### Difficile
#### Normale

### 2021-09-13
#### Difficile
#### Normale

### 2021-09-14
#### Difficile
#### Normale

### 2021-09-15
#### Difficile
#### Normale

### 2021-09-16
#### Difficile
#### Normale

### 2021-09-17
#### Difficile
#### Normale
