# Locandina Draft

## Titolo

Maker Camp 2021

## Descrizione

L'agenzia **MakerSpaceY** sta reclutando figure di ogni tipo (ingegneri, architetti, informatici, designer, musicisti, filosofi, ecc) per il viaggio interplanetario Terra-Marte TM2021. La durata della missione è di 3 settimane dal **05** al **24 Luglio**.

Per iscriverti vai all'indirizzo [bit.ly/maker-camp-2021](https://bit.ly/maker-camp-2021) oppure scannerizza il QR code.

![QR Code](./qr-code.svg)

### Week #1

Preparativi per la partenza, progettazione missione, addestramento e decollo.

### Week #2

Viaggio spaziale, manutenzione astronave ed atterraggio su Marte.

### Week #3

Integrazione nella società marziana, sopravvivenza, costruzione ed esplorazione.

## Sponsor

- Makerspace Fabriano
- PDP Free Software User Group
- F-Actory
- Biblioteca Multimediale Fabriano
- DOORS
- [Make](https://make.co/join/?utm_source=makercamp)
  
