# Lettera di benvenuto

Salve!
Siamo felici di incontrarti e accoglierti nella nostra nuova agenzia di viaggi MakerSpaceY.  
Avevamo proprio bisogno di te per organizzare un viaggio spaziale.  Ogni giorno ci sarà un'attività diversa che servirà per preparare il viaggio e la progettazione della nuova base marziana. 
Sperimenteremo cose nuove ed interessanti: inventeremo e costruiremo un'astronave, un razzo, una base spaziale, capiremo come comunicare nello spazio e progetteremo l'allenamento per i cosmoturisti.

Potrai partecipare alla missione anche per un giorno solo, ma ti consigliamo di essere presente per tutta la durata del Camp. Potrai partecipare in presenza i giorni 12 e 13 luglio e dal 19 al 23 luglio oppure online tutti i giorni dalle 15:00 alle 18:00.

Attività in presenza 
Le attività si svolgeranno presso F-Factory  via Veneto 50 a Fabriano.

Attività Online
Le attività online saranno sempre supportate. Utilizzeremo una piattaforma molto innovativa che ci permetterà di vivere anche online l'esperienza di un makerspace.
Link di accesso: 
https://gather.town/app/El7BVnJ2c2yZFivw/Makerspace_Fabriano
PASSWORD : makercamp21
Appena entri apri il microfono e presentati. Sarai accolto da uno del nostro staff. 

Se hai bisogno di comunicare con noi o hai bisogno di ulteriori informazioni puoi chiamare il  329 2643176 (Andrei)

A presto! 
Per aspera ad astra
Andrei, Costin e Beatrice

Ecco il programma per la prima settimana:

Week #1 Preparazione del viaggio

Day #1  lunedì 12 luglio - L'astronave
Luogo: F-Factory Fabriano e Gather.Town
Guest star: Mauro Gagliardi

Introduzione al camp, conoscenza tra camperisti . 
Formazioni degli ingegneri e degli interior designer dell'agenzia MakerSpaceY
Ideazione, progettazione, costruzione delle astronavi. 

Day #2  martedì 13 luglio - Volare  OH OH!
Luogo: F-Factory Fabriano e Gather.Town
Guest Star: Maurizio Lucarini

Costruzione di paracaduti, aerei, aquiloni

Day #3  mercoledì 14 luglio - Comunicare ...---...
Luogo: Gather.Town
Guest Star: Adriano Parracciani

Riusciamo a sentire la Stazione Spaziale internazionale?
Costruzione di un'antenna.
Preparazione di un concerto su Marte (ci riusciremo?)

Day #4 giovedì 15 luglio - Stupidità Artificiale
Luogo: Gather.Town
Guest stars: Francesco Coppola, Dawid Węglarz, Valerio Mariani, Lorenzo Farinelli

Creazione un servizio di chatbot per l'agenzia MakerSpaceY

Day #5 - venerdì 16 luglio - Un Allenamento Spaziale
Luogo: Gather.Town

Definizione del programma di allenamento dei cosmoturisti. 
Ideazione e progettazione degli attrezzi per la allenamento dei cosmoturisti.
