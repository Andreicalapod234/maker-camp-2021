# Maker Camp 2021

L'agenzia MakerspaceY sta organizzando un viaggio turistico su Marte. Durante il maker camp prepareremo tutto il necessario per partire: l'astronave, il vettore, le attrezzature, i programmi di allenamento per i cosmoturisti e infine la stazione marziana.

I maker camp sono laboratori gratuiti offerti a giovani e meno giovani per promuovere la creatività sotto ogni forma, con particolare attenzione alle discipline STEM (Scienze, Tecnologia, Ingegneria, Matematica), in modo da stimolarli a sviluppare una mentalità da maker.
I makers sono persone curiose a cui piace esplorare e condividere nuove strade della conoscenza. 
Le attività di making permettono ai ragazzi in età scolare di sperimentare l'apprendimento tramite l'utilizzo delle proprie mani e sviluppare un atteggiamento positivo verso i propri fallimenti vivendoli come opportunità.

Il Make camp si svolgerà dal 12 al 23 Luglio dalle 15 alle 18 in maniera ibrida, online e in presenza. 

Durante le due settimane svolgeremo attività che coinvolgeranno la manualità e il mondo digitale.  Verranno utilizzati e sperimentati diverse piattaforme per la programmazione e la progettazione come Scratch, Blender, open rocket, KSP e tanto altro ancora.

Week #1 Preparazione

Day #1  lunedì 12 luglio - L'astronave
Luogo: F-Factory Fabriano e GatherTown
Guest star: Mauro Gagliardi

Introduzione al camp, conoscenza tra camperisti . 
Formazioni degli ingegneri e degli interior designer dell'agenzia MakerSpaceY
Ideazione, progettazione, costruzione delle astronavi. 

Day #2  martedì 13 luglio - Volare  
Luogo: F-Factory Fabriano e GatherTown
Guest Star: Maurizio Lucarini

Costruzione di paracaduti, aerei, aquiloni

Day #3  mercoledì 14 luglio - Comunicare nello spazio
Luogo: GatherTown
Guest Star: Adriano Parracciani

Riusciamo a sentire la Stazione Spaziale internazionale?
Costruzione di un'antenna.
Preparazione di un concerto su Marte (ci riusciremo?)

Day #4 giovedì 15 luglio - Stupidità artificiale
Luogo: GatherTown
Guest stars: Francesco Coppola, Dawid Węglarz, Valerio Mariani, Lorenzo Farinelli

Creazione un servizio di chatbot per l'agenzia

Day #5 - venerdì 16 luglio - Allenamento spaziale
Luogo: GatherTown

Ideazione e progettazione degli attrezzi per la allenamento dei cosmoturisti
Definizione del programma di allenamento dei cosmoturisti 

 
Week #2 Viaggio e soggiorno

Day #1  lunedì 19 luglio - Shuttle o rocket that is the question?
Luogo: F-Factory Fabriano e GatherTown

Azione e reazione.
Progettazione e Costruzione del vettore.

Day #2  martedì 20 luglio - Villaggio vacanze Marte
Luogo: F-Factory Fabriano e GatherTown

Design e costruzione della stazione marziana

Day #3  mercoledì 21 luglio - Sala Giochi
Luogo: F-Factory Fabriano e GatherTown

Creazione di giochi e attività in una stazione marziale

Day #4  giovedì 22 luglio - Trasporti marziani
Luogo: F-Factory Fabriano e GatherTown

Progettazione, costruzione e programmazione di rover su Marte

Day #5 - venerdì 23 luglio - Pitch del Viaggio   
Luogo: F-Factory Fabriano e GatherTown

Benchmark sui viaggi turistici spaziali
Costruzione della biglietteria per i cosmonauti 
Preparazione di un logo
Preparazione della campagna pubblicitaria, trailer e locandina
Preparazione della rampa di lancio del vettore
